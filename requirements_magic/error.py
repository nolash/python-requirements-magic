class ExistsError(Exception):
    pass


class VersionError(Exception):
    pass
